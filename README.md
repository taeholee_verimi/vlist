# VList
Coding challenge for Verimi

## Env.

- Xcode 10.3 + Swift
- Working time: 3~4 hr
- Added some xctests for APIs

## Screenshots

### Portrait

<img width="20%" src="https://www.evernote.com/l/AEFJTHdosglBJ7N0rvSHvRbdY1pQMWnZC-YB/image.png">

### Landscape

<img width="50%" src="https://www.evernote.com/l/AEHFnETo68BPjInffoN2ikgK8_GSbHncCdAB/image.png">


### Delete

<img width="20%" src="https://www.evernote.com/l/AEGVB3g20S5H2LfCXDWXfZw1dq2QPkZOihMB/image.png">


### Create

<img width="20%" src="https://www.evernote.com/l/AEHhOyLm3Y9B1YKvOvWjvrIDGH2YTx43SuoB/image.png">


### Detail View

<img width="20%" src="https://www.evernote.com/l/AEHEASUIrcRAWpqYHpIpBKU46mJvF61h8ucB/image.png">

### iPad Split

<img width="50%" src="https://www.evernote.com/l/AEE0D86dCG5HA44Y_D3gUCuMRJfVRdVg4k4B/image.png">

### iPad Split - Landscape

<img width="50%" src="https://www.evernote.com/l/AEEbjfGtf-tExa74yw8h8WDIhr0cERVvI_QB/image.png">
