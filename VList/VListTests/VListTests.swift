//
//  VListTests.swift
//  VListTests
//
//  Created by Taeho Lee on 9/20/19.
//  Copyright © 2019 Verimi GmbH. All rights reserved.
//

import XCTest
@testable import VList

class VListTests: XCTestCase {

    let manager = VDataManager()

    func testItemsData() {
        let e = expectation(description: "URL client would complete to fetch list without an error.")

        manager.fetchItems { items, error in
            XCTAssertNil(error, error?.localizedDescription ?? "")
            XCTAssertTrue(items != nil && items?.count ?? 0 > 0)
            e.fulfill()
        }
        waitForExpectations(timeout: 30) { _ in XCTAssertTrue(true) }
    }

    func testItemData() {
        let e = expectation(description: "URL client would complete to fetch an item without an error.")

        manager.fetchItem(id: "5d5fb1df2b7cf03000009576") { item, error in
            XCTAssertNil(error, error?.localizedDescription ?? "")
            XCTAssertTrue(item != nil)
            e.fulfill()
        }
        waitForExpectations(timeout: 30) { _ in XCTAssertTrue(true) }
    }

    func testCreateItemData() {
        let e = expectation(description: "URL client would complete to fetch an item without an error.")

        let newItem = VItem(_id: "", displayName: "Verimi test", description: "I'm now working", imageUrl: "https://verimi.de/static/PhoneInHand-69ea51b081aa4d5c083a302ae14bcac5.png")

        manager.createItem(item: newItem) { error in
            XCTAssertNil(error, error?.localizedDescription ?? "")
            e.fulfill()
        }
        waitForExpectations(timeout: 30) { _ in XCTAssertTrue(true) }
    }
}
