//
// Copyright (c) 2019 Verimi GmbH. All rights reserved.
//

import Foundation

struct VItem: Codable {
    let _id: String
    let displayName: String?
    let description: String?
    let imageUrl: String?

    // For excluding '_id'. If '_id' contains in POST, error.
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(displayName, forKey: .displayName)
        try container.encode(description, forKey: .description)
        try container.encode(imageUrl, forKey: .imageUrl)
    }
}

class VDataManager {

    private let baseURI = "https://verimitest-4446.restdb.io/rest/service-providers"

    private func makeRequest(pathComponent: String = "", method: String) -> URLRequest? {
        guard let url = URL(string: baseURI + (pathComponent.count>0 ? "/\(pathComponent)" : "")) else {
            return nil
        }

        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("460888f4c77f2cba6c7fbb5e6be5f6cdf0937", forHTTPHeaderField: "x-apikey")
        request.httpMethod = method

        return request
    }

    func fetchItems(completion: @escaping ([VItem]?, Error?) -> Swift.Void)  {
        guard let request = makeRequest(method:"GET") else {
            completion(nil, "Couldn't make a request.")
            return
        }

        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if let error = error {
                completion(nil, error)
                return
            }

            guard let data = data else {
                completion(nil, "Data is nil")
                return
            }

            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .secondsSince1970
                let items = try decoder.decode([VItem].self, from: data)
                completion(items, nil)
            } catch let e {
                completion(nil, e)
            }
        }.resume()
    }

    func fetchItem(id: String, completion: @escaping (VItem?, Error?) -> Swift.Void)  {
        guard let request = makeRequest(pathComponent: id, method:"GET") else {
            completion(nil, "Couldn't make a request.")
            return
        }

        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if let error = error {
                completion(nil, error)
                return
            }

            guard let data = data else {
                completion(nil, "Data is nil")
                return
            }

            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .secondsSince1970
                let item = try decoder.decode(VItem.self, from: data)
                completion(item, nil)
            } catch let e {
                completion(nil, e)
            }
        }.resume()
    }

    func deleteItem(id: String, completion: @escaping (Error?) -> Swift.Void)  {
        guard let request = makeRequest(pathComponent: id, method:"DELETE") else {
            completion("Couldn't make a request.")
            return
        }

        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if let error = error {
                completion(error)
                return
            }

            guard let data = data else {
                completion("Data is nil")
                return
            }

            print(String(data: data, encoding: .utf8) as Any)
            completion(nil)

        }.resume()
    }

    func createItem(item: VItem, completion: @escaping (Error?) -> Swift.Void)  {
        guard let request = makeRequest(method:"POST") else {
            completion("Couldn't make a request.")
            return
        }

        var mutableRequest = request

        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        do {
            let jsonData = try encoder.encode(item)
            mutableRequest.httpBody = jsonData
            print("JSON String : " + String(data: jsonData, encoding: .utf8)!)

            URLSession.shared.dataTask(with: mutableRequest) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
                if let error = error {
                    completion(error)
                    return
                }

                guard let data = data else {
                    completion("Data is nil")
                    return
                }

                print(String(data: data, encoding: .utf8))

                completion(error)
            }.resume()

        } catch let e {
            completion(e)
        }
    }
}

extension String: Error {}