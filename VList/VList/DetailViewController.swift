//
//  DetailViewController.swift
//  VList
//
//  Created by Taeho Lee on 9/20/19.
//  Copyright © 2019 Verimi GmbH. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    static let segueIndentifier = "showDetail"

    lazy var detailDescriptionTextView = UITextView()
    lazy var detailImageView = UIImageView()
    lazy var formView = FormView()

    var editMode: Bool = false {
        didSet {
            if editMode {
                configureViewForEditMode()
            } else {
                configureViewForDisplayMode()
            }
        }
    }

    var editedDetailItem: VItem? {
        get {
            if editMode{
                let displayName = formView.nameField.text ?? ""

                return VItem(_id: "", displayName: displayName.count > 0 ? displayName : "Untitled", description: formView.descriptionField.text, imageUrl: formView.urlField.text)
            }else {
                return nil
            }
        }
    }

    var detailItem: VItem? {
        didSet {
            if detailItem != nil {
                configureViewForDisplayMode()
            }
        }
    }

    func configureViewForEditMode() {
        view.subviews.forEach { $0.removeFromSuperview() }

        view.addSubview(formView)
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        formView.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
        formView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    func configureViewForDisplayMode() {
        view.subviews.forEach { $0.removeFromSuperview() }

        let containerView = UIView()
        view.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        if let detail = detailItem {

            if let imageUrl = detail.imageUrl {
                containerView.addSubview(detailImageView)
                detailImageView.backgroundColor = .lightGray
                detailImageView.contentMode = .scaleAspectFit
                detailImageView.translatesAutoresizingMaskIntoConstraints = false
                detailImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
                detailImageView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
                detailImageView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
                detailImageView.heightAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true

                containerView.addSubview(detailDescriptionTextView)
                detailDescriptionTextView.translatesAutoresizingMaskIntoConstraints = false
                detailDescriptionTextView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
                detailDescriptionTextView.topAnchor.constraint(equalTo: detailImageView.bottomAnchor, constant: 20).isActive = true
                detailDescriptionTextView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true

                loadImage(url: imageUrl)

            } else {
                containerView.addSubview(detailDescriptionTextView)
                detailDescriptionTextView.translatesAutoresizingMaskIntoConstraints = false
                detailDescriptionTextView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
                detailDescriptionTextView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
                detailDescriptionTextView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
            }

            detailDescriptionTextView.textContainer.maximumNumberOfLines = 0
            detailDescriptionTextView.textContainer.lineBreakMode = .byWordWrapping
            detailDescriptionTextView.text = detail.description
            detailDescriptionTextView.sizeToFit()

            navigationItem.title = detail.displayName
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureViewForDisplayMode()
    }

    private func loadImage(url: String) {
        guard let url = URL(string: url) else {
            return
        }

        URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            if error != nil {
                return
            }

            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                return
            }

            guard let data = data else {
                return
            }

            DispatchQueue.main.async {
                self?.detailImageView.image = UIImage(data: data)
            }
        }.resume()
    }
}

