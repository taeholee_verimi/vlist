//
//  MasterViewController.swift
//  VList
//
//  Created by Taeho Lee on 9/20/19.
//  Copyright © 2019 Verimi GmbH. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController?

    lazy var addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(enterEditor(_:)))
    lazy var indicator = UIActivityIndicatorView(style: .gray)

    var refreshingTimer: Timer?

    var items = [VItem]()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = editButtonItem
        navigationItem.rightBarButtonItem = addButton

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        tableView.refreshControl = refreshControl

        loadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        refreshingTimer = Timer.scheduledTimer(withTimeInterval: 30, repeats: true) { [weak self] timer in
            self?.loadData()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        refreshingTimer?.invalidate()
        refreshingTimer = nil
    }

    func startIndicating(){
        indicator.startAnimating()
        indicator.hidesWhenStopped = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: indicator)
    }

    func stopIndicating(){
        indicator.stopAnimating()
        navigationItem.rightBarButtonItem = addButton
    }

    @objc
    func loadData() {
        startIndicating()

        VDataManager().fetchItems { [weak self] items, error in
            if let items = items, error == nil {
                DispatchQueue.main.async {
                    self?.items = items
                    self?.stopIndicating()
                    self?.tableView.refreshControl?.endRefreshing()
                    self?.tableView.reloadData()
                }
            }
        }
    }

    @objc
    func enterEditor(_ sender: Any) {
        performSegue(withIdentifier: DetailViewController.segueIndentifier, sender: sender)
    }

    @objc
    func saveButtonTapped(_ sender: Any) {
        guard let item = seguedDetailViewController?.editedDetailItem else{
            return
        }

        CATransaction.begin()
        CATransaction.setCompletionBlock {
            self.startIndicating()
            VDataManager().createItem(item: item) { [weak self] error in
                if error == nil {
                    DispatchQueue.main.async {
                        self?.items.insert(item, at: 0)
                        self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                        self?.stopIndicating()
                    }
                }
            }
        }
        navigationController?.popViewController(animated: true)
        CATransaction.commit()
    }

    // MARK: - Segues

    weak var seguedDetailViewController: DetailViewController?

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == DetailViewController.segueIndentifier {
            let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController

            if (sender as? UIBarButtonItem) == addButton {
                controller.editMode = true
                controller.navigationItem.title = "Create"
                controller.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveButtonTapped(_:)))

            } else {

                if let indexPath = tableView.indexPathForSelectedRow {
                    let item = items[indexPath.row]
                    controller.detailItem = item
                    controller.navigationItem.title = item.displayName
                }
            }

            controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            controller.navigationItem.leftItemsSupplementBackButton = true

            seguedDetailViewController = controller
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = items[indexPath.row]
        cell.textLabel!.text = object.displayName
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let targetId = items[indexPath.row]._id

            startIndicating()

            VDataManager().deleteItem(id: targetId) { [weak self] error in
                DispatchQueue.main.async {
                    self?.items.remove(at: indexPath.row)
                    self?.tableView.deleteRows(at: [indexPath], with: .fade)
                    self?.stopIndicating()
                }
            }

        } else if editingStyle == .insert {

        }
    }


}

