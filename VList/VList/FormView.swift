//
// Copyright (c) 2019 Verimi GmbH. All rights reserved.
//

import Foundation
import UIKit

class FormView: UIView{
    private(set) var nameField = UITextField()
    private(set) var urlField = UITextField()
    private(set) var descriptionField = UITextField()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initialize()
    }

    func initialize() {
        addSubview(nameField)

        addSubview(urlField)

        addSubview(descriptionField)
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        nameField.placeholder = "Display Name"
        nameField.translatesAutoresizingMaskIntoConstraints = false
        nameField.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        nameField.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        nameField.widthAnchor.constraint(equalTo: widthAnchor, constant: -20).isActive = true
        nameField.heightAnchor.constraint(equalToConstant: 20).isActive = true
        nameField.borderStyle = .roundedRect

        urlField.placeholder = "https://imageurl.com"
        urlField.translatesAutoresizingMaskIntoConstraints = false
        urlField.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        urlField.topAnchor.constraint(equalTo: nameField.bottomAnchor, constant: 10).isActive = true
        urlField.widthAnchor.constraint(equalTo: widthAnchor, constant: -20).isActive = true
        urlField.heightAnchor.constraint(equalToConstant: 20).isActive = true
        urlField.borderStyle = .roundedRect

        descriptionField.translatesAutoresizingMaskIntoConstraints = false
        descriptionField.placeholder = "Description"
        descriptionField.widthAnchor.constraint(equalTo: widthAnchor, constant: -20).isActive = true
        descriptionField.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        descriptionField.topAnchor.constraint(equalTo: urlField.bottomAnchor, constant: 10).isActive = true
        descriptionField.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        descriptionField.borderStyle = .roundedRect
        descriptionField.contentVerticalAlignment = .top
    }
}
